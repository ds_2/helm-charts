# How to build charts

Create a chart via:

    helm create MYCHARTNAME

## Test it

    helm lint MYCHARTNAME

## Package it

    helm package src/MYCHARTNAME

## Create global index file

    helm repo index --url https://mydomain .
