# DS/2 Helm Charts

Some little helm charts to use.

## Our Repo

Unless otherwise specified, the repo is here:

    helm repo add ds2 https://ds2.jfrog.io/artifactory/helms/
    helm repo update
    helm search repo ds2

## How to create new charts

    helm create CHARTNAME

## How to test

Set your test environment:

    kubectx ds2
    kubens infra

or use KinD:

    kind create cluster --name helmtest --image kindest/node:v1.19.11 --wait
    kubectl cluster-info --context kind-helmtest
    kubectx kind-helmtest
    kubectl create ns infra

Then, rollout your chart with a test config (which could be empty, of course):

    helm lint githubactionrunner
    helm template githubactionrunner
    helm test githubactionrunner # this assumes that you installed the helm chart!!
    helm upgrade -i --namespace infra ghartest githubactionrunner/ --values githubactionrunner/values.yaml -f githubactionrunner/localtest.yaml
    helm upgrade -i --namespace infra uc1 urlchecker/ --values urlchecker/values.yaml -f urlchecker/localtest.yaml
    helm dependency update boundary/; helm dependency build boundary/
    helm upgrade -i --namespace infra boundary boundary/ --values boundary/values.yaml --wait --timeout 1m

### Unittesting with a plugin

Install the plugin:

    helm plugin install https://github.com/quintush/helm-unittest

Run the test:

    helm unittest -3 githubactionrunner

## Releasing the chart

We allow only releases from the main branch. Run the ops shell script:

    export MYCHART=CHARTNAME
    export VERSION=1.2.3

Then run this:

    ./helmOps.sh -c $MYCHART -o test
    ./helmOps.sh -c $MYCHART -v $VERSION -o release

## Cleanup kind

    kind delete cluster kind-helmtest
