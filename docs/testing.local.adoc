= Testing Helm Charts locally

In case you have your own KUBECONFIG set to a cluster nearby, you can run this command to rollout the helm chart to the target cluster:

[,shell]
----
CHARTNAME='test-chart-01'
CHART='./pvc-tester'
TGT_NS='infra-test-01'
kubectl create ns $TGT_NS || true
helm upgrade -i -n $TGT_NS $CHARTNAME $CHART --set keepPvc=true
----

To uninstall it, run:

[,shell]
----
helm uninstall -n $TGT_NS $CHARTNAME
----
