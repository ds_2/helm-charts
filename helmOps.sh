#!/usr/bin/env bash

CHART=""
OPERATION=""
VERSION=""
DESTDIR=$(pwd)/target
HELM_SERVER_URL=${HELM_SERVER_URL:-https://ds2.jfrog.io/artifactory/helms/}
helmServerUserName=${HELM_SERVER_USERNAME:-ci}
helmServerUserPw=${HELM_SERVER_USERPW}

while getopts ':c:o:v:' OPTION; do
    case "$OPTION" in
    c) CHART="$OPTARG" ;;
    o) OPERATION="$OPTARG" ;;
    v) VERSION="$OPTARG" ;;
    *) echo "Unbekannter Parameter" ;;
    \?)
        echo "Invalid param: $OPTARG"
        exit 1
        ;;
    esac
done

curl -L ${HELM_SERVER_URL}index.yaml -o $DESTDIR/currentIndex.yaml

case "$OPERATION" in
test)
    helm lint ${CHART}
    helm template ${CHART}
    helm test ${CHART}
    helm unittest ${CHART}
    ;;
rollout)
    helm upgrade -i --namespace infra ${CHART}test ${CHART}/ --values ${CHART}/values.yaml -f ${CHART}/localtest.yaml
    ;;
release)
    THISBRANCH=$(git branch --show-current)
    if [ ! "main" == $THISBRANCH ]; then
        echo "You are on the wrong branch (${THISBRANCH}). We only allow releases to the official repo from the main branch! Feature branches use the test way deployment for the chart ;) See README.md."
        exit 1
    fi
    if [ -z "$helmServerUserName" ]; then
        echo "No username defined for push user! Use HELM_SERVER_USERNAME env var!"
        exit 1
    fi
    if [ -z "$helmServerUserPw" ]; then
        echo "No pw defined for push user! Use HELM_SERVER_USERPW env var!"
        exit 1
    fi
    helm package ${CHART} --version "$VERSION" --destination $DESTDIR
    THISFILE="${DESTDIR}/${CHART}-${VERSION}.tgz"
    sed -i '' "s/^version: .*/version: ${VERSION}/g" ${CHART}/Chart.yaml || exit 2
    git add ${CHART}/Chart.yaml
    echo "Uploading to server(s).."
    curl -u ${helmServerUserName}:${helmServerUserPw} $HELM_SERVER_URL --upload-file ${THISFILE} || exit 1
    echo "Finishing release.."
    git commit -m "[release] Releasing ${CHART} $VERSION"
    tagName="${CHART}-${VERSION}"
    git tag "${tagName}"
    echo "Perhaps you want to run: $0 -o refresh"
    ;;
refresh)
    helm repo update
    helm search repo ds2
    ;;
*)
    # unknown
    echo "No action defined!"
    ;;
esac
echo "Done"
