# Sample configs

This folder contains dummy secrets, configmaps etc to test a helm chart in the KIND cluster.

## Database

Run this:

    helm install my-db2 --set postgresql.password=test1235,postgresql.database=boundarydb bitnami/postgresql-ha
